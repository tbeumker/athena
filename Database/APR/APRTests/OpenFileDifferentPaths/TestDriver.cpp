//  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration

/* "import" TestDriver class code  fromt the PersistencySvc_FileOpenWithoutCatalog test
   which provides DB creation and reading functionality
*/
#include "../PersistencySvc_FileOpenWithoutCatalog/TestDriver.cpp"
