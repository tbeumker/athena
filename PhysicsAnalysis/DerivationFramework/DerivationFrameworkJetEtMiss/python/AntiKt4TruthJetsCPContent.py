# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration

AntiKt4TruthJetsCPContent = [
"AntiKt4TruthJets",
"AntiKt4TruthJetsAux.pt.eta.phi.m.PartonTruthLabelID.HadronConeExclExtendedTruthLabelID.HadronConeExclTruthLabelID.TrueFlavor"
]

