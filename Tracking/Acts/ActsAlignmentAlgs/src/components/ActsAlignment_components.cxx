/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#include "../DetectorAlignCondAlg.h"
#include "../GeometryContextAlg.h"
#include "../AlignStoreProviderAlg.h"

DECLARE_COMPONENT(ActsTrk::AlignStoreProviderAlg)
DECLARE_COMPONENT(ActsTrk::GeometryContextAlg)
DECLARE_COMPONENT(ActsTrk::DetectorAlignCondAlg)
